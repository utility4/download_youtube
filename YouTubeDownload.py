# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 12:42:17 2020

@author: Untamed Yeti
"""

#This program will download videos from Youtube
from pytube import YouTube

#Select video
video_url = 'https://www.youtube.com/watch?v=_t773h9UolY'

#Select location
location = r'C:\YouTube\Video'

#download 
YouTube(video_url).streams.first().download(location)
